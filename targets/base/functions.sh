
declare -A KERNEL_CMDLINE_PARAMS
parse_cmdline() {
    local name value
    set -- $(cat /proc/cmdline)
    for x in "$@"; do
        case "$x" in
            debug)
                # Use FD 19 to capture the debug stream caused by "set -x":
                exec 19>/var/log/live/livebootp-startup.log
                # Tell bash about it  (there's nothing special about 19, its arbitrary)
                export BASH_XTRACEFD=19
                # turn on the debug stream:
                set -x
                ;;
            root-password-disabled)
                passwd -d root
                ;;
        esac
        name=${x%%=*}
        if [ "$x" == "$name" ]; then
            KERNEL_CMDLINE_PARAMS[$name]="true"
        else
            KERNEL_CMDLINE_PARAMS[$name]=$(eval "echo ${x#$name=}")
        fi
    done
}

parse_cmdline

# Print error message and exit
# * argument 1: Message
print_critical() {
    printf "\e[91m[CRIT] %s\e[0m\n" "$@"
    return 1
}

# Print error message
# * argument 1: Message
print_error() {
    printf "\e[91m[ERRO] %s\e[0m\n" "$@"
}

# print note message
# * argument 1: Message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Print info message
# * argument 1: Message
print_info() {
    printf "\e[92m[INFO] %s\e[0m\n" "$@"
}

# Print a warning message
print_warning() {
    >&2 printf "\e[93m[WARN] %s\e[0m\n" "$@"
}

# Get a /proc/cmdline parameter
# * argument 1: parameter name
get_cmdline_param() {
    echo "${KERNEL_CMDLINE_PARAMS[$1]}"
}

LIVEBOOTP_FETCH_URL_CURL_ADD_OPTS=()
if [ "${KERNEL_CMDLINE_PARAMS[fetch-curl-allow-insecure]}" == "true" ]; then
    LIVEBOOTP_FETCH_CURL_ADD_OPTS+=(--insecure)
fi

# Fetch a ressource
# * argument 1: URL
# * argument 2: target file path
fetch_url() {
    local url=$1
    local tg=$2
    local url_proto url_hostname url_portnum url_path
    eval "$(echo "$1" | gawk '
        match($0, /^(\w+):\/\/([^\/:]+)(:(\d*))?(\S+)$/, res) {
            printf "url_proto=%s\n \
                url_hostname=%s\n \
                url_portnum=%s\n \
                url_path=%s\n", res[1], res[2], res[3], res[5]
        }')"
    case "$url_proto" in
        tftp)
            tftp "$url_hostname" -c get "$url_path" "$tg"
            ;;
        http|https)
            curl "${LIVEBOOTP_FETCH_URL_CURL_ADD_OPTS[@]}" --location --silent --output "$tg" "$url"
            ;;
        *)
            print_critical "fetch_url: unsupported protocol $url_proto"
            ;;
    esac
}

# Test if mount point is active
# * argument 1: target mount point
is_mounted() {
    return $(awk '
        BEGIN {
            retcode = 1;
        }
        ($2 == "'"$1"'") {
            retcode=0;
            exit;
        }
        END {
            print retcode;
        }' /etc/mtab)
}

# Test if two files have the same content
# * argument 1: file 1
# * argument 2: file 2
is_identical_file () {
    return $(sha256sum "$1" "$2" | awk '
        BEGIN {
            retcode = 1;
        }
        (sha256==$1) {
            retcode=0;
            exit;
        }
        {
            sha256=$1
        }
        END {
            print retcode;
        }')
}
